package test.model;

import presentation.model.presentation.Presentation;
import presentation.model.slide.Slide;
import presentation.model.slideComponents.TextComponent;
import presentation.utile.SaveManager;
import presentation.view.PresentationFrameView;


public class SerialisationTest {
    public static void main(String[] args) {
        int i = 0;

        Presentation presentation = new Presentation();
        Slide slide1 = new Slide();
        TextComponent textComponent1 =new TextComponent("blabla");
        Slide slide2 = new Slide();
        TextComponent textComponent2 =new TextComponent("bloublou");
        presentation.getSlide(0).addComponent(textComponent1);

        slide1.addComponent(textComponent1);
        slide1.addComponent(textComponent1);
        slide1.addComponent(textComponent1);
        slide1.addComponent(textComponent1);
        slide2.addComponent(textComponent2);

        presentation.addSlide(slide1);
        presentation.addSlide(slide2);

        SaveManager saveManager = new SaveManager("Serialiser.xml",presentation);
        saveManager.savePresentation();
        saveManager.loadPresentation();
        Presentation presentation2 = saveManager.getPresentation();

        System.out.println("nombre de Slide"+presentation2.getSlideList().size());

        for (Slide slide: presentation2.getSlideList()) {
            System.out.println("nombre de composant du slide"+i+" : "+slide.getSlideComponentList().size());
            i++;
        }
        PresentationFrameView presentationFrameView = new PresentationFrameView(presentation);

    }



}
