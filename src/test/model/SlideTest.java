package test.model;

import presentation.model.slide.Slide;
import presentation.model.slideComponents.TextComponent;

public class SlideTest {
    public static void main(String[] args) {

    }

    public static Slide emptySlide()
    {
        return new Slide();
    }

    public static Slide textSlide()
    {
        Slide slide = new Slide();

        TextComponent textComponent = SlideComponentTest.textComponent();

        slide.addComponent(textComponent);

        return slide;
    }
}
