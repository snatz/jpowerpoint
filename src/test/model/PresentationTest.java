package test.model;

import presentation.model.presentation.Presentation;

public class PresentationTest {
    public static Presentation emptyPresentation()
    {
        return new Presentation();
    }

    public static Presentation textPresentation()
    {
        Presentation presentation = emptyPresentation();

        return presentation;
    }
}
