package test.view;

import presentation.model.presentation.Presentation;
import presentation.model.slide.Slide;
import presentation.model.slideComponents.TextComponent;
import presentation.view.VignetteListView;

import javax.swing.*;

public class VignetteListContainerTest {
    public static void main(String[] args) {
        Presentation presentation = new Presentation();
        Slide slide1 = new Slide();
        TextComponent textComponent1 =new TextComponent("blabla");
        Slide slide2 = new Slide();
        TextComponent textComponent2 =new TextComponent("bloublou");
        presentation.getSlide(0).addComponent(textComponent1);

        slide1.addComponent(textComponent1);
        slide1.addComponent(textComponent1);
        slide1.addComponent(textComponent1);
        slide1.addComponent(textComponent1);
        slide2.addComponent(textComponent2);

        presentation.addSlide(slide1);
        presentation.addSlide(slide2);

        JFrame test = new EmptyFrame();
        VignetteListView vignetteListView = new VignetteListView(presentation);
        test.add(vignetteListView);
        test.pack();
    }
}
