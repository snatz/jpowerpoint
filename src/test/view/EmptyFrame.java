package test.view;

import javax.swing.*;
import java.awt.*;

public class EmptyFrame extends JFrame {
    public EmptyFrame() throws HeadlessException {
        super("Empty");
        setSize(600,600);
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
