package test.view;

import presentation.factory.ViewFactory;
import presentation.model.presentation.Presentation;
import test.model.PresentationTest;

public class PresentationFrameTest {
    public static void main(String[] args) {
        Presentation presentation = PresentationTest.textPresentation();
        ViewFactory.createPresentationFrameView(presentation);
    }
}
