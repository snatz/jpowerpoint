package test.view;

import presentation.controller.slideComponents.ImageComponentController;
import presentation.controller.slideComponents.TextComponentController;
import presentation.model.slideComponents.ImageComponent;
import presentation.model.slideComponents.TextComponent;
import presentation.view.slideComponents.ImageComponentView;
import presentation.view.slideComponents.TextComponentView;

import java.io.IOException;

public class SlideComponentTest {
    public static void main(String[] args) throws IOException {
        TextComponent textComponent = new TextComponent();
        textComponent.setText("test");
        ImageComponent imageComponent = new ImageComponent();
        imageComponent.setImagePath("images/surfer.jpg");

        TextComponentView textComponentView = new TextComponentView(textComponent);
        new TextComponentController(textComponentView);

        ImageComponentView imageComponentView = new ImageComponentView(imageComponent);
        new ImageComponentController(imageComponentView);

        EmptyFrame frame = new EmptyFrame();
        frame.add(textComponentView);
        frame.add(imageComponentView);

    }
}
