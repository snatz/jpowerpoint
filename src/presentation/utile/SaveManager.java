package presentation.utile;

import presentation.model.presentation.Presentation;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class SaveManager{
    private Presentation presentation;
    private final String filename;
    private XMLEncoder encoder = null;
    private XMLDecoder decoder = null;

    public SaveManager(String filename, Presentation presentation) {
        this.filename = filename;
        this.presentation = presentation;
    }
    public void loadPresentation() {
        try {
            this.decoder = new XMLDecoder(new FileInputStream(this.filename));
            this.presentation = (Presentation) this.decoder.readObject();
        } catch (FileNotFoundException fnfe) {
            System.out.println("Load: FileNotFoundException: " + fnfe.getMessage());
        } finally {
            if (this.decoder != null) {
                this.decoder.close();
            }
        }
    }

    public Presentation getPresentation() {
        return presentation;
    }

    public void savePresentation() {
        try {
            this.encoder = new XMLEncoder(new FileOutputStream(this.filename));
            this.encoder.writeObject(this.presentation);
            this.encoder.flush();
        } catch (FileNotFoundException fnfe) {
            System.out.println("Update: File not found exception: " + fnfe.getMessage());
        } finally {
            if (this.encoder != null) {
                this.encoder.close();
            }
        }
    }
}
