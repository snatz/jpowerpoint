package presentation.view;

import presentation.model.slide.Slide;
import presentation.view.slideComponents.SlideComponentView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;

public class SlideView extends JPanel{

    private Slide slide;
    private List<SlideComponentView> slideComponentViewsList = new ArrayList<>();

    public static float miniatureZoomFactor = 0.15f;

    public static final Dimension INITIAL_DIMENSION = new Dimension(1080, 720);

    private boolean miniature;

    public SlideView(Slide slide) {
        this(slide, false);
    }

    public SlideView(Slide slide, boolean miniature) {
        this.slide = slide;
        this.miniature = miniature;

        setBackground(Color.white);
        setLayout(null);

        setPreferredSize(INITIAL_DIMENSION);

        update();
    }


    public void update() {
        updateComponentList();

        if(isMiniature())
            updateBorder();
        else
            setPreferredSize(new Dimension((int)(INITIAL_DIMENSION.getWidth()*SlideContainerView.zoomRatio),(int)(INITIAL_DIMENSION.getHeight()*SlideContainerView.zoomRatio)));

        revalidate();
        repaint();
    }

    private void updateComponentList() {
        removeAll();

        for (SlideComponentView slideComponentView : slideComponentViewsList) {
            slideComponentView.update();
            add(slideComponentView);
        }

    }

    private void updateBorder() {
        /*
        if (slide.isCurrentSlide()) {
            setBorder(BorderFactory.createLineBorder(Color.black, 4));
        } else {
            setBorder(BorderFactory.createLineBorder(Color.black, 2));
        }
        */
    }

    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);

        if(miniature)
        {
            Graphics2D g2 = (Graphics2D) g;

            if(slide.isCurrentSlide()) { // Border replacement for selected miniatures
                g2.setStroke(new BasicStroke(4));
                g2.drawRect(0, 0, getSize().width, getSize().height);
            }

            AffineTransform affineTransform = new AffineTransform();
            affineTransform.setToScale(miniatureZoomFactor, miniatureZoomFactor);
            g2.transform(affineTransform);
        }
    }

    public void onFocus() {
        updateBorder();
    }

    public void onFocusLost() {
        updateBorder();
    }

    public Slide getSlide() {
        return slide;
    }

    public void setSlide(Slide slide) {
        this.slide = slide;
    }

    public List<SlideComponentView> getSlideComponentViewsList() {
        return slideComponentViewsList;
    }

    public void setSlideComponentViewsList(List<SlideComponentView> slideComponentViewsList) {
        this.slideComponentViewsList = slideComponentViewsList;
    }

    public boolean isMiniature() {
        return miniature;
    }

    public void setMiniature(boolean miniature) {
        this.miniature = miniature;
    }

    public Dimension getMiniatureDimension() {
        return new Dimension((int) Math.floor(INITIAL_DIMENSION.getWidth() * miniatureZoomFactor), (int) Math.floor(INITIAL_DIMENSION.getHeight() * miniatureZoomFactor));
    }
}
