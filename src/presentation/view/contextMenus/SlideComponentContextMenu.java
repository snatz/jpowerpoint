package presentation.view.contextMenus;

import presentation.view.slideComponents.SlideComponentView;

import javax.swing.*;

public class SlideComponentContextMenu extends ContextMenu {
    public SlideComponentContextMenu(SlideComponentView slideComponentView, int x, int y) throws IllegalStateException {
        super(slideComponentView.getSlideComponent().getSlide().getPresentation(), x, y);
        JMenuItem item;

        setMenuX(x);
        setMenuY(y);

        addSeparator();

        item = new JMenuItem("Delete");
        item.addActionListener(actionEvent -> slideComponentView.getSlideComponent().remove());

        add(item);
    }
}
