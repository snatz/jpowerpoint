package presentation.view.contextMenus;

import presentation.model.actions.CreateSlideAction;
import presentation.model.actions.CreateTextAction;
import presentation.model.presentation.Presentation;

import javax.swing.*;

public class ContextMenu extends JPopupMenu {
    private Presentation presentation;
    private int menuX, menuY;

    public ContextMenu(Presentation presentation, int x, int y) {
        this.presentation = presentation;
        this.menuX = x;
        this.menuY = y;

        JMenu newMenu;
        JMenuItem item;

        newMenu = new JMenu("New");

        item = new JMenuItem("Slide");
        item.addActionListener(actionEvent -> {
            CreateSlideAction createSlideAction = new CreateSlideAction(getPresentation());
            getPresentation().getActionStack().push(createSlideAction);
        });
        newMenu.add(item);

        item = new JMenuItem("Text");
        item.addActionListener(actionEvent -> {
            CreateTextAction createTextAction = new CreateTextAction(presentation, menuX, menuY);
            getPresentation().getActionStack().push(createTextAction);
        });
        newMenu.add(item);

        add(newMenu);
    }

    public Presentation getPresentation() {
        return presentation;
    }

    public void setPresentation(Presentation presentation) {
        this.presentation = presentation;
    }

    public int getMenuX() {
        return menuX;
    }

    public void setMenuX(int menuX) {
        this.menuX = menuX;
    }

    public int getMenuY() {
        return menuY;
    }

    public void setMenuY(int menuY) {
        this.menuY = menuY;
    }
}
