package presentation.view.contextMenus;

import presentation.view.SlideView;

import javax.swing.*;

public class VignetteContextMenu extends ContextMenu {
    public VignetteContextMenu(SlideView vignetteView, int x, int y) throws IllegalStateException {
        super(vignetteView.getSlide().getPresentation(), x, y);
        JMenuItem item;

        setMenuX(x);
        setMenuY(y);

        addSeparator();

        item = new JMenuItem("Delete");
        item.addActionListener(actionEvent -> vignetteView.getSlide().remove());
        add(item);
    }
}
