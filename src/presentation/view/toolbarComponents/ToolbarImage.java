package presentation.view.toolbarComponents;

import javax.swing.*;

public class ToolbarImage extends JToolBar{
    private JButton rotation;

    public ToolbarImage(){
        super();

        rotation = new JButton(new ImageIcon("images/rotate.png"));
        add(rotation);
        addSeparator();
        add(new JButton("Others actions"));
        addSeparator();
    }

    public JButton getRotation() {
        return rotation;
    }
}
