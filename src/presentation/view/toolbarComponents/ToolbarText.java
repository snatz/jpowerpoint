package presentation.view.toolbarComponents;

import javax.swing.*;
import java.awt.*;

public class ToolbarText extends JToolBar{
    private JButton colorChooser;
    private Color currentColor = Color.BLACK; // By default, the color font selected is black

    private JTextField fontSizeArea;
    private Integer currentFontSize = 11; // By default, the font size is fixed to 11

    private JComboBox<String> fontCombo;
    private String currentFontName = "Arial"; // By default, the font selected is Arial

    private JButton italicButton;
    private boolean isItalic;

    private JButton boldButton;
    private boolean isBold;

    private JButton underlineButton;
    private boolean isUnderline;


    public ToolbarText(){
        super();

        addJTextField(this);
        addSeparator();

        addJComboBox(this);
        addSeparator();

        addJButton(this);
        addSeparator();
    }

    private void addJButton(JToolBar jToolBar){
        colorChooser = new JButton("Choose color font");
        colorChooser.setBackground(currentColor);
        jToolBar.add(colorChooser);

        italicButton = new JButton("Italic");
        jToolBar.add(italicButton);

        boldButton = new JButton("Bold");
        jToolBar.add(boldButton);

        underlineButton = new JButton("Underline");
        jToolBar.add(underlineButton);
    }

    private void addJTextField(JToolBar jToolBar){
        fontSizeArea = new JTextField(this.currentFontSize.toString());
        fontSizeArea.setColumns(4);
        fontSizeArea.setMaximumSize(fontSizeArea.getPreferredSize());
        jToolBar.add(fontSizeArea);
    }

    private void addJComboBox(JToolBar jToolBar){
        //String fontsName[] = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();

        String[] fontsName = {"Arial","Calibri","Century","Comic sans MS","Courier New","Helvetica","Impact","Serif","Times New Roman","Trebuchet MS"};

        fontCombo = new JComboBox<>(fontsName);
        fontCombo.setSelectedItem(0);

        JScrollPane fontsListScrollPane = new JScrollPane(fontCombo);
        fontsListScrollPane.setMaximumSize(new Dimension(150,100));
        jToolBar.add(fontsListScrollPane);
    }

    public JComboBox<String> getFontCombo() { return fontCombo; }

    public JButton getColorChooser() { return colorChooser; }

    public JButton getItalicButton() { return italicButton; }

    public JButton getBoldButton() { return boldButton; }

    public JButton getUnderlineButton() { return underlineButton; }

    public JTextField getFontSizeArea() { return fontSizeArea; }

    public Integer getCurrentFontSize() { return currentFontSize; }

    public void setCurrentFontSize(Integer currentFontSize) { this.currentFontSize = currentFontSize; }

    public Color getCurrentColor() { return currentColor; }

    public void setCurrentColor(Color currentColor) { this.currentColor = currentColor; }

    public String getCurrentFontName() { return currentFontName; }

    public void setCurrentFontName(String currentFontName) { this.currentFontName = currentFontName; }

    public void setItalic(boolean italic) { isItalic = italic;}

    public boolean isItalic() { return isItalic; }

    public boolean isBold() { return isBold; }

    public void setBold(boolean bold) { isBold = bold;}

    public boolean isUnderline() { return isUnderline; }

    public void setUnderline(boolean underline) { isUnderline = underline; }
}
