package presentation.view.toolbarComponents;

import presentation.model.presentation.Presentation;

import javax.swing.*;
import java.awt.*;

public class Toolbar extends JPanel{
    private JToolBar jToolBar;
    private ToolbarText jToolBarText;
    private ToolbarImage jToolBarImage;

    private JButton addSlide;
    private JButton addText;
    private JButton addImg;

    private Presentation presentation;

    public Toolbar(Presentation presentation) {
        super(new BorderLayout(5,5));

        this.presentation = presentation;

        jToolBarText = new ToolbarText();
        jToolBarImage = new ToolbarImage();

        jToolBar = new JToolBar("Toolbar");
        jToolBar.setFloatable(true);
        jToolBar.setRollover(true);

        addJButtons(jToolBar);
        jToolBar.addSeparator();

        jToolBar.add(jToolBarText);
        jToolBar.add(jToolBarImage);

        // By default, we don't display toolbarText & toolbarImg
        jToolBarText.setVisible(false);
        jToolBarImage.setVisible(false);

        add(jToolBar, BorderLayout.NORTH);
    }



    private void addJButtons(JToolBar jToolBar){
        addSlide = new JButton("Add slide");
        jToolBar.add(addSlide);

        addText = new JButton("Add text area");
        jToolBar.add(addText);

        addImg = new JButton("Add image");
        jToolBar.add(addImg);
    }

    public ToolbarText getjToolBarText() { return jToolBarText; }

    public ToolbarImage getjToolBarImg() { return jToolBarImage; }

    public JButton getAddSlide() { return addSlide; }

    public JButton getAddImg() {return addImg; }

    public JButton getAddText() { return addText; }

    public Presentation getPresentation() { return presentation; }

}
