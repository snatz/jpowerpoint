package presentation.view;

import presentation.controller.PresentationAdapter;
import presentation.model.presentation.Presentation;

import javax.swing.*;
import java.awt.*;

public class PresentationFrameView extends JFrame {

    private Presentation presentation;

    private final PresentationMenuView presentationMenuView;
    private final PresentationContainerView presentationContainerView;

    public PresentationFrameView(Presentation presentation) {
        this.presentation = presentation;

        presentationContainerView = new PresentationContainerView(presentation, this);
        presentationMenuView = new PresentationMenuView(presentation);

        instantiateFrame();
    }

    private void instantiateFrame() {
        setJMenuBar(presentationMenuView);
        add(presentationContainerView);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(Toolkit.getDefaultToolkit().getScreenSize());
        setTitle("JPowerPoint");
        setVisible(true);
        addComponentListener(new PresentationAdapter(this, this.getSize()));
    }

    public Presentation getPresentation() {
        return presentation;
    }

    public PresentationMenuView getPresentationMenuView() {
        return presentationMenuView;
    }

    public PresentationContainerView getPresentationContainerView() {
        return presentationContainerView;
    }
}
