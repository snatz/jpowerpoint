package presentation.view;

import presentation.model.presentation.Presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SlideContainerView extends JPanel {
    private Presentation presentation;
    private SlideView slideView;

    protected static double  zoomRatio;

    public SlideContainerView(Presentation presentation) {
        this.presentation = presentation;
    }

    public Presentation getPresentation() {
        return presentation;
    }

    public void update(){
        if(slideView != null)
            slideView.update();
        updateSize();
        revalidate();
        repaint();
    }

    public void setSlideView(SlideView slideView) {
        this.slideView = slideView;
    }

    public SlideView getSlideView() {
        return slideView;
    }

    public double getZoomRatio() {
        return zoomRatio;
    }

    public void setZoomRatio(double zoomRatio) {
        this.zoomRatio = zoomRatio;
    }

    public void updateSize(){
        Dimension theoricSize = new Dimension((int)(slideView.INITIAL_DIMENSION.getWidth()*zoomRatio) , (int)(slideView.INITIAL_DIMENSION.getHeight()*zoomRatio));
        setPreferredSize(theoricSize);
    }
}