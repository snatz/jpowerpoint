package presentation.view.slideComponents;

import presentation.model.slideComponents.TextComponent;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;

public class TextComponentView extends SlideComponentView {
    private TextComponent textComponent;
    private JLabel label = new JLabel();
    private JTextPane textEditable = new JTextPane();
    private boolean editing = false;
    private String originalText;

    public TextComponentView(TextComponent textComponent) {
        this(textComponent, false);
    }

    public TextComponentView(TextComponent textComponent, boolean miniature) {
        super(textComponent);

        setMiniature(miniature);

        setLayout(new BorderLayout());
        setOpaque(false);

        this.textComponent = textComponent;

        label.setVerticalAlignment(SwingConstants.TOP);
        label.setHorizontalAlignment(SwingConstants.LEFT);
        add(label, BorderLayout.CENTER);

        update();
    }

    @Override
    public void update() {
        super.update();
        updateText();

        revalidate();
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g){
        Graphics2D g2d;
        if (g instanceof Graphics2D) {
            g2d = (Graphics2D) g;
        } else {
            return;
        }
        g2d.scale(getZoomFactor(),getZoomFactor());
        super.paintComponent(g);
        Font f = getStyleFont();
        Color color = textComponent.getFontColor();
        label.setFont(f);
        label.setForeground(color);
        textEditable.setFont(f);
        textEditable.setForeground(color);
    }

    private Font getStyleFont() {
        Font f;
        boolean isItalic = textComponent.isItalic();
        boolean isBold = textComponent.isBold();
        boolean isUnderline = textComponent.isUnderline();

        if (isItalic && isBold) {
            f = new Font(textComponent.getFontName(), Font.ITALIC | Font.BOLD, textComponent.getFontSize());
        } else if (isItalic) {
            f = new Font(textComponent.getFontName(), Font.ITALIC, textComponent.getFontSize());
        } else if (isBold) {
            f = new Font(textComponent.getFontName(), Font.BOLD, textComponent.getFontSize());
        } else {
            f = new Font(textComponent.getFontName(), Font.PLAIN, textComponent.getFontSize());
        }

        this.originalText = label.getText();

        if (isUnderline) {
            label.setText("<html><u>" + originalText + "</u></html>");
        } else {
            label.setText(originalText);
        }

        return f;
    }


    public void updateText() {
        if (!Objects.equals(textComponent.getText(), label.getText()))
            label.setText(textComponent.getText());

        if (!Objects.equals(textComponent.getText(), textEditable.getText()))
            textEditable.setText(textEditable.getText());

        setFont(getStyleFont());
    }


    public void edit() {
        if(editing) return;
        editing = true;

        remove(label);
        add(textEditable, BorderLayout.CENTER);

        revalidate();
        repaint();
    }

    public void endEdition() {
        if(!editing) return;
        editing = false;

        textComponent.setText(textEditable.getText());

        remove(textEditable);
        add(label, BorderLayout.CENTER);

        revalidate();
        repaint();
    }

    public boolean isEditing() {
        return editing;
    }

    public void setEditing(boolean editing) {
        this.editing = editing;
    }

    public TextComponent getTextComponent() {
        return textComponent;
    }

    public void setTextComponent(TextComponent textComponent) {
        this.textComponent = textComponent;
    }

}
