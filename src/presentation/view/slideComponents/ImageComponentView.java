package presentation.view.slideComponents;

import presentation.model.slideComponents.ImageComponent;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageComponentView extends SlideComponentView {
    private ImageComponent imageComponent;
    private BufferedImage image = null;

    public ImageComponentView(ImageComponent imageComponent) {
        this(imageComponent, false);
    }

    public ImageComponentView(ImageComponent imageComponent, boolean miniature) {
        super(imageComponent);
        this.imageComponent = imageComponent;

        setMiniature(miniature);
        setLayout(null);

        update();
    }

    public ImageComponent getImageComponent() {
        return imageComponent;
    }

    public void setImageComponent(ImageComponent imageComponent) {
        this.imageComponent = imageComponent;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d;
        if (g instanceof Graphics2D) {
            g2d = (Graphics2D) g;
        } else {
            return;
        }
        super.paintComponent(g);
        g2d.clearRect(0, 0, getWidth(), getHeight());
        if (imageComponent.getImagePath() != null) {
            if (image == null) {
                try {
                    image = ImageIO.read(new File(imageComponent.getImagePath()));
                    g2d.drawImage(image, 0, 0, getWidth(), getHeight(), this);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                g2d.drawImage(image, 0, 0, getWidth(), getHeight(), this);
            }
        } else {
            g2d.setColor(getBackground());
            g2d.fillRect(0, 0, getWidth(), getHeight());
        }
    }

    public void update() {
        super.update();
    }

    public void updatePosition(){
    }

}
