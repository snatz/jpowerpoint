package presentation.view.slideComponents;

import presentation.model.slideComponents.SlideComponent;

import javax.swing.*;
import java.awt.*;

abstract public class SlideComponentView extends JPanel {
    private SlideComponent slideComponent;
    private Point anchorPoint;
    private Cursor draggingCursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
    private double zoomFactor = 1 ;

    private boolean miniature = false;

    private boolean draggable = true;

    // if overbearing is true, when dragging component, it will be painted over each other
    private boolean overbearing = true;

    public SlideComponentView(SlideComponent slideComponent) {
        this.slideComponent = slideComponent;
    }

    public void update() {
        updateSize();
        updateLocation();
        updateBorder();
        revalidate();
        repaint();
    }


    private void updateSize() {
        Point position = slideComponent.getPosition();
        Dimension size = slideComponent.getSize();
        if(zoomFactor<=1)
            setBounds(position.x, position.y, (int)(size.width*zoomFactor), (int)(size.height*zoomFactor));
        else
            setBounds(position.x, position.y, size.width, size.height);
    }

    public void updateLocation(){
        if (slideComponent.getPosition() != null) {
            double x = slideComponent.getPosition().getX()*zoomFactor;
            double y = slideComponent.getPosition().getY()*zoomFactor;
            Point point = new Point((int)x,(int)y);
            setLocation(point);
        }
    }

    public void updateBorder() {
        if (slideComponent.isFocused() && !isMiniature()) // Don't put borders on miniatures
            setBorder(BorderFactory.createDashedBorder(Color.black,2.1f,3,2.5f, false));
        else
            setBorder(null);
    }

    public double getZoomFactor() {
        return zoomFactor;
    }

    public void setZoomFactor(double zoomFactor) {
        this.zoomFactor = zoomFactor;
    }

    public SlideComponent getSlideComponent() {
        return slideComponent;
    }

    public void setSlideComponent(SlideComponent slideComponent) {
        this.slideComponent = slideComponent;
    }

    public Cursor getDraggingCursor() {
        return draggingCursor;
    }

    public void setDraggingCursor(Cursor draggingCursor) {
        this.draggingCursor = draggingCursor;
    }

    public Point getAnchorPoint() {
        return anchorPoint;
    }

    public void setAnchorPoint(Point anchorPoint) {
        this.anchorPoint = anchorPoint;
    }

    public boolean isDraggable() {
        return draggable;
    }

    public void setDraggable(boolean draggable) {
        this.draggable = draggable;
    }

    public boolean isOverbearing() {
        return overbearing;
    }

    public void setOverbearing(boolean overbearing) {
        this.overbearing = overbearing;
    }

    public boolean isMiniature() {
        return miniature;
    }

    public void setMiniature(boolean miniature) {
        this.miniature = miniature;
    }
}
