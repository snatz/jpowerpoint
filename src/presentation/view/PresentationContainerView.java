package presentation.view;

import presentation.model.presentation.Presentation;
import presentation.view.toolbarComponents.Toolbar;

import javax.swing.*;
import java.awt.*;

public class PresentationContainerView extends JPanel {

    private Presentation presentation;

    private final JFrame frame;
    private final Toolbar toolbar;
    private final VignetteListView vignetteListView;
    private final SlideContainerView slideContainerView;
    private final JScrollPane scrollPane;

    public PresentationContainerView(Presentation presentation, JFrame frame) {
        this.presentation = presentation;
        this.frame = frame;

        toolbar = new Toolbar(presentation);
        vignetteListView = new VignetteListView(presentation);
        slideContainerView = new SlideContainerView(presentation);
        JPanel panel = new JPanel();
        panel.add(slideContainerView);
        scrollPane = new JScrollPane(panel);
        scrollPane.setPreferredSize(slideContainerView.getSize());
        initContainer();
    }

    private void initContainer() {
        setLayout(new BorderLayout());

        add(toolbar, BorderLayout.NORTH);
        add(vignetteListView, BorderLayout.WEST);
        add(scrollPane, BorderLayout.CENTER);
        setVisible(true);
    }

    public Presentation getPresentation() {
        return presentation;
    }

    public VignetteListView getVignetteListView() {
        return vignetteListView;
    }

    public SlideContainerView getSlideContainerView() {return slideContainerView;}

    public Toolbar getToolbar() {return toolbar; }

    public JFrame getFrame() {
        return frame;
    }
}
