package presentation.view;

import presentation.model.presentation.Presentation;

import javax.swing.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

public class PresentationMenuView extends JMenuBar{
    private Presentation presentation;
    private JMenu file;
    private JMenuItem newPres,savePres,loadPres;

    PresentationMenuView(Presentation presentation) {
        this.presentation = presentation;
        file = new JMenu("File");
        file.setMnemonic(KeyEvent.VK_F);

        newPres = new JMenuItem("New");
        newPres.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));

        savePres = new JMenuItem("Save");
        savePres.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));

        loadPres = new JMenuItem("Open");
        loadPres.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));

        file.add(newPres);
        file.add(savePres);
        file.add(loadPres);

        this.add(file);
    }

    public JMenuItem getNewPres() {
        return newPres;
    }

    public JMenuItem getSavePres() {
        return savePres;
    }

    public JMenuItem getLoadPres() {
        return loadPres;
    }

    public Presentation getPresentation() {
        return presentation;
    }
}
