package presentation.view;

import presentation.model.presentation.Presentation;
import presentation.model.slide.Slide;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class VignetteListView extends JPanel{
    private Presentation presentation;
    private List<SlideView> vignetteViewList = new ArrayList<>();

    public VignetteListView(Presentation presentation) {
        this.presentation = presentation;

        setLayout(new BorderLayout());

        update();
    }

    public Presentation getPresentation() {
        return presentation;
    }

    public List<SlideView> getVignetteViewList() {
        return vignetteViewList;
    }

    public void update() {
        removeAll();
        JPanel panel = new JPanel();

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));


        int listWidth = 0; // (int)(VignetteView.vignetteSize.getWidth()*2);
        int listHeight = 0; // (int)(VignetteView.vignetteSize.getHeight());



        for (SlideView vignette : vignetteViewList){
            listWidth = (int) Math.max(listWidth, vignette.getMiniatureDimension().getWidth());
            listHeight += vignette.getMiniatureDimension().getHeight();
            panel.add(vignette);
            panel.add(Box.createRigidArea(new Dimension(0,10)));

        }

        panel.setPreferredSize(new Dimension(listWidth, listHeight));

        JScrollPane scrollPane = new JScrollPane(panel);

        if (vignetteViewList.size() == 0) {
            panel.add(new JLabel("Aucun slide à afficher"));
        } else {
            scrollPane.setPreferredSize(new Dimension(listWidth + 20, Math.min(listHeight, 900) + 20));
        }

        add(scrollPane,BorderLayout.NORTH);

        revalidate();
        repaint();
    }

    public SlideView getSlideView(Slide slide)
    {
        for(SlideView slideView : vignetteViewList)
        {
            if(slideView.getSlide() == slide)
                return slideView;
        }

        return null;
    }

    public void setVignetteViewList(List<SlideView> vignetteViewList) {
        this.vignetteViewList = vignetteViewList;
    }
}
