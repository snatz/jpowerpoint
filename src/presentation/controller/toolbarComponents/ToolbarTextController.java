package presentation.controller.toolbarComponents;

import presentation.model.actions.ChangeTextAction;
import presentation.model.slide.Slide;
import presentation.model.slideComponents.SlideComponent;
import presentation.model.slideComponents.TextComponent;
import presentation.view.toolbarComponents.ToolbarText;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class ToolbarTextController {
    private ToolbarText textToolBarView;
    private ToolbarController toolbarController;

    public ToolbarTextController(ToolbarText textToolBarView,ToolbarController toolbarController) {
        this.textToolBarView = textToolBarView;
        this.toolbarController = toolbarController;

        textToolBarView.getColorChooser().addActionListener(e -> {
            Color newColor = JColorChooser.showDialog(textToolBarView, "Choose font color", textToolBarView.getCurrentColor());
            List<SlideComponent> slideComponentFocusedList = toolbarController.getListenedSlide().getSelection().getSlideComponentList();
            for(SlideComponent slideComponentFocused : slideComponentFocusedList){
                TextComponent textComponent = getTextComponentFocused(slideComponentFocused);
                if (textComponent != null) {
                    ChangeTextAction changeTextAction = new ChangeTextAction(textComponent, textToolBarView, "color",newColor);
                    slideComponentFocused.getSlide().getPresentation().getActionStack().push(changeTextAction);
                }
            }
        });

        textToolBarView.getFontSizeArea().addActionListener(e -> {
            List<SlideComponent> slideComponentFocusedList = toolbarController.getListenedSlide().getSelection().getSlideComponentList();
            for(SlideComponent slideComponentFocused : slideComponentFocusedList){
                TextComponent textComponent = getTextComponentFocused(slideComponentFocused);
                if (textComponent != null) {
                    ChangeTextAction changeTextAction = new ChangeTextAction(textComponent, textToolBarView, "size",textComponent.getFontColor());
                    slideComponentFocused.getSlide().getPresentation().getActionStack().push(changeTextAction);
                }
            }
        });

        textToolBarView.getFontCombo().addItemListener(e -> {
            List<SlideComponent> slideComponentFocusedList = toolbarController.getListenedSlide().getSelection().getSlideComponentList();
            for(SlideComponent slideComponentFocused : slideComponentFocusedList){
                TextComponent textComponent = getTextComponentFocused(slideComponentFocused);
                if (textComponent != null) {
                    ChangeTextAction changeTextAction = new ChangeTextAction(textComponent, textToolBarView, "font",textComponent.getFontColor());
                    slideComponentFocused.getSlide().getPresentation().getActionStack().push(changeTextAction);
                }
            }
        });

        textToolBarView.getItalicButton().addActionListener(e -> {
            List<SlideComponent> slideComponentFocusedList = toolbarController.getListenedSlide().getSelection().getSlideComponentList();
            SlideComponent slideComponent = slideComponentFocusedList.get(0);
            TextComponent textComponent = getTextComponentFocused(slideComponent);
            if (textComponent != null) { // we change the view button only once time
                if(textComponent.isItalic()){
                    textToolBarView.getItalicButton().setBackground(null);
                }
                else
                {
                    textToolBarView.getItalicButton().setBackground(Color.RED);
                }
                // However we apply the action on all the component selected
                for(SlideComponent slideComponentFocused : slideComponentFocusedList){
                    TextComponent textComponentFocused = getTextComponentFocused(slideComponentFocused);
                    ChangeTextAction changeTextAction = new ChangeTextAction(textComponentFocused, textToolBarView, "italic",textComponent.getFontColor());
                    slideComponentFocused.getSlide().getPresentation().getActionStack().push(changeTextAction);
                }
            }
        });

        textToolBarView.getBoldButton().addActionListener(e -> {
            List<SlideComponent> slideComponentFocusedList = toolbarController.getListenedSlide().getSelection().getSlideComponentList();
            SlideComponent slideComponent = slideComponentFocusedList.get(0);
            TextComponent textComponent = getTextComponentFocused(slideComponent);
            if (textComponent != null) { // we change the view button only once time
                if (textToolBarView.isBold()) {
                    textToolBarView.getBoldButton().setBackground(null);
                } else {
                    textToolBarView.getBoldButton().setBackground(Color.RED);
                }
            }
            // However we apply the action on all the component selected
            for(SlideComponent slideComponentFocused : slideComponentFocusedList) {
                TextComponent textComponentFocused = getTextComponentFocused(slideComponentFocused);
                ChangeTextAction changeTextAction = new ChangeTextAction(textComponentFocused, textToolBarView, "bold",textComponent.getFontColor());
                slideComponentFocused.getSlide().getPresentation().getActionStack().push(changeTextAction);
            }
        });

        textToolBarView.getUnderlineButton().addActionListener(e -> {
            List<SlideComponent> slideComponentFocusedList = toolbarController.getListenedSlide().getSelection().getSlideComponentList();
            SlideComponent slideComponent = slideComponentFocusedList.get(0);
            TextComponent textComponent = getTextComponentFocused(slideComponent);
            if (textComponent != null) { // we change the view button only once time
                if (textToolBarView.isUnderline()) {
                    textToolBarView.getUnderlineButton().setBackground(null);
                } else {
                    textToolBarView.getUnderlineButton().setBackground(Color.RED);
                }
            }
            // However we apply the action on all the component selected
            for(SlideComponent slideComponentFocused : slideComponentFocusedList) {
                TextComponent textComponentFocused = getTextComponentFocused(slideComponentFocused);
                ChangeTextAction changeTextAction = new ChangeTextAction(textComponentFocused, textToolBarView, "underline",textComponent.getFontColor());
                slideComponentFocused.getSlide().getPresentation().getActionStack().push(changeTextAction);
            }
        });
    }

    private TextComponent getTextComponentFocused(SlideComponent slideComponentFocused) {
        if (slideComponentFocused instanceof TextComponent) {
            return (TextComponent)slideComponentFocused;
        } else  {
            return null;
        }
    }
}
