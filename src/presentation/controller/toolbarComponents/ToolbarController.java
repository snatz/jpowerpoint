package presentation.controller.toolbarComponents;

import presentation.model.actions.CreateImageAction;
import presentation.model.actions.CreateTextAction;
import presentation.model.presentation.PresentationListener;
import presentation.model.slide.Slide;
import presentation.model.slide.SlideListener;
import presentation.model.slideComponents.ImageComponent;
import presentation.model.slideComponents.SlideComponent;
import presentation.model.slideComponents.TextComponent;
import presentation.view.toolbarComponents.Toolbar;
import presentation.view.toolbarComponents.ToolbarText;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.util.List;

public class ToolbarController implements SlideListener, PresentationListener{
    private Toolbar toolbarView;
    private Slide listenedSlide;
    private boolean textFocused = false;
    private boolean imageFocused = false;

    public ToolbarController(Toolbar toolbarView) {
        this.toolbarView = toolbarView;

        new ToolbarTextController(toolbarView.getjToolBarText(),this);
        new ToolbarImageController(toolbarView.getjToolBarImg(),this);

        toolbarView.getPresentation().addPresentationListener(this);

        listenSlide(toolbarView.getPresentation().getCurrentSlide());

        // Listening events
        toolbarView.getAddSlide().addActionListener(e -> toolbarView.getPresentation().addSlide(new Slide()));

        toolbarView.getAddText().addActionListener(e -> {
            if(listenedSlide == null)
                return;

            CreateTextAction createTextAction = new CreateTextAction(listenedSlide.getPresentation(), 10, 10);
            listenedSlide.getPresentation().getActionStack().push(createTextAction);
        });

        toolbarView.getAddImg().addActionListener(e -> {
            if (listenedSlide == null)
                return;

            JFileChooser chooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG Images", "jpg");
            chooser.setFileFilter(filter);
            if (chooser.showOpenDialog(toolbarView) == JFileChooser.APPROVE_OPTION) {
                CreateImageAction createImageAction = new CreateImageAction(
                        toolbarView.getPresentation(),
                        chooser.getSelectedFile().getPath(),
                        10,
                        10
                );
                toolbarView.getPresentation().getActionStack().push(createImageAction);
            }
        });

        updateView();
    }

    private void listenSlide(Slide slide) {
        if(listenedSlide != null)
            listenedSlide.removeSlideListener(this);

        listenedSlide = slide;

        if (listenedSlide != null) {
            listenedSlide.addSlideListener(this);
        }
    }

    private void updateView(){
        showCorrespondingToolbars();
    }

    private void showCorrespondingToolbars(){
        toolbarView.getjToolBarText().setVisible(textFocused);
        toolbarView.getjToolBarImg().setVisible(imageFocused);
    }


    @Override
    public void onSlideComponentAdded(SlideComponent slideComponent){}

    @Override
    public void onSlideComponentRemoved(SlideComponent slideComponent){}

    @Override
    public void onSlideComponentModified(SlideComponent slideComponent) {

    }

    @Override
    public void onSelectionChanged() {
        textFocused = false;
        imageFocused = false;
        List<SlideComponent> slideComponentsList = listenedSlide.getSelection().getSlideComponentList();

        for (SlideComponent slideComponent : slideComponentsList) {
            if (slideComponent instanceof TextComponent)
                textFocused = true;

            if (slideComponent instanceof ImageComponent)
                imageFocused = true;
        }

        if (textFocused) { // if we have many component focused, we send the datas of the first in the list to the toolbar
            TextComponent firstTextComponent = (TextComponent)slideComponentsList.get(0);
            ToolbarText toolbarText = toolbarView.getjToolBarText();

            toolbarText.getFontSizeArea().setText(Integer.toString(firstTextComponent.getFontSize()));
            toolbarText.getColorChooser().setBackground(firstTextComponent.getFontColor());

            if (firstTextComponent.isBold())
                toolbarText.getBoldButton().setBackground(Color.RED);
            else
                toolbarText.getBoldButton().setBackground(null);

            if (firstTextComponent.isUnderline())
                toolbarText.getUnderlineButton().setBackground(Color.RED);
            else
                toolbarText.getUnderlineButton().setBackground(null);

            if (firstTextComponent.isItalic())
                toolbarText.getItalicButton().setBackground(Color.RED);
            else
                toolbarText.getItalicButton().setBackground(null);

            toolbarText.getFontCombo().setSelectedItem(firstTextComponent.getFontName());
        }

        updateView();
    }

    @Override
    public void onFocus(){}

    @Override
    public void onFocusLost(){}

    @Override
    public void onSlideAdded(Slide slide) {}

    @Override
    public void onSlideRemoved(Slide slide) {}

    @Override
    public void onSlideFocused(Slide slide) {
        listenSlide(slide);
        updateView();
    }

    public Slide getListenedSlide() { return listenedSlide; }
}
