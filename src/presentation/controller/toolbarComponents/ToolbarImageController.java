package presentation.controller.toolbarComponents;

import presentation.model.slideComponents.ImageComponent;
import presentation.model.slideComponents.SlideComponent;
import presentation.view.toolbarComponents.ToolbarImage;

import java.util.List;


public class ToolbarImageController{
    private ToolbarImage toolbarImageView;
    private ToolbarController toolbarController;

    public ToolbarImageController(ToolbarImage toolbarImageView, ToolbarController toolbarController){
        this.toolbarImageView = toolbarImageView;
        this.toolbarController = toolbarController;
        toolbarImageView.getRotation().addActionListener(e -> {
            List<SlideComponent> slideComponentListFocused = toolbarController.getListenedSlide().getSelection().getSlideComponentList();
            for(SlideComponent slideComponentFocused : slideComponentListFocused) {
                if(slideComponentFocused instanceof ImageComponent){
                    ImageComponent imageComponent = (ImageComponent) slideComponentFocused;
                    imageComponent.rotateImage();
                }
            }
        });
    }

}
