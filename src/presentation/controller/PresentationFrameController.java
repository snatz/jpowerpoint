package presentation.controller;

import presentation.view.PresentationContainerView;
import presentation.view.PresentationFrameView;
import presentation.view.PresentationMenuView;

public class PresentationFrameController {
    private PresentationFrameView presentationFrameView;

    public PresentationFrameController(PresentationFrameView presentationFrameView) {
        this.presentationFrameView = presentationFrameView;

        PresentationContainerView presentationContainerView = presentationFrameView.getPresentationContainerView();
        PresentationMenuView presentationMenuView = presentationFrameView.getPresentationMenuView();

        PresentationContainerController presentationContainerController = new PresentationContainerController(presentationContainerView);
        PresentationMenuController presentationMenuController = new PresentationMenuController(presentationMenuView);
    }
}
