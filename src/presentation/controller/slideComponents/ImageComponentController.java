package presentation.controller.slideComponents;

import presentation.model.slideComponents.ImageComponentListener;
import presentation.view.slideComponents.ImageComponentView;

public class ImageComponentController extends SlideComponentController implements ImageComponentListener {
    ImageComponentView imageComponentView;

    public ImageComponentController(ImageComponentView imageComponentView) {
        super(imageComponentView);

        this.imageComponentView = imageComponentView;

        imageComponentView.getImageComponent().addImageComponentListener(this);
        imageComponentView.getImageComponent().addSlideComponentListener(this);
    }

    @Override
    public void onImageChange() {
        imageComponentView.update();
    }

}
