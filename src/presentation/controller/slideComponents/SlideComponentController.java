package presentation.controller.slideComponents;

import presentation.model.actions.MoveComponentAction;
import presentation.model.slide.Slide;
import presentation.model.slideComponents.SlideComponent;
import presentation.model.slideComponents.SlideComponentListener;
import presentation.view.contextMenus.SlideComponentContextMenu;
import presentation.view.slideComponents.SlideComponentView;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Objects;

abstract public class SlideComponentController implements SlideComponentListener {
    private SlideComponentView slideComponentView;

    public SlideComponentController(SlideComponentView slideComponentView) {
        this.slideComponentView = slideComponentView;

        if (slideComponentView.isDraggable() && !slideComponentView.isMiniature()) {
            SlideComponentMouseListener slideComponentMouseListener = new SlideComponentMouseListener();

            slideComponentView.addMouseListener(slideComponentMouseListener);
            slideComponentView.addMouseMotionListener(slideComponentMouseListener);
        }

        slideComponentView.getSlideComponent().addSlideComponentListener(this);
    }

    private void updateView() {
        slideComponentView.update();
    }

    @Override
    public void onFocus() {
        slideComponentView.update();
    }

    @Override
    public void onFocusLost() {
        slideComponentView.update();
    }

    @Override
    public void onRemoved() {
    }

    @Override
    public void onModified() {
        slideComponentView.update();
    }

    private class SlideComponentMouseListener extends MouseAdapter {
        private boolean dragging = false;
        private Point dragLocation, parentOnScreen, mouseOnScreen, position;

        @Override
        public void mouseDragged(MouseEvent mouseEvent) {
            if (!dragging)
                return;

            int anchorX = slideComponentView.getAnchorPoint().x;
            int anchorY = slideComponentView.getAnchorPoint().y;

            parentOnScreen = slideComponentView.getParent().getLocationOnScreen();
            mouseOnScreen = mouseEvent.getLocationOnScreen();
            position = new Point(mouseOnScreen.x - parentOnScreen.x - anchorX, mouseOnScreen.y - parentOnScreen.y - anchorY);

            switch (slideComponentView.getCursor().getType()) {
                case (Cursor.SE_RESIZE_CURSOR): {
                    for (SlideComponent component : slideComponentView.getSlideComponent().getSlide().getSelection().getSlideComponentList())
                        component.setSize(new Dimension(
                            (int)(slideComponentView.getWidth() + mouseEvent.getPoint().getX() - dragLocation.getX()),
                            (int)(slideComponentView.getHeight() + mouseEvent.getPoint().getY() - dragLocation.getY())
                    ));

                    dragLocation = mouseEvent.getPoint();
                    updateView();
                    break;
                }

                case (Cursor.E_RESIZE_CURSOR): {
                    for (SlideComponent component : slideComponentView.getSlideComponent().getSlide().getSelection().getSlideComponentList()) {
                        component.setSize(new Dimension(
                                (int) (slideComponentView.getWidth() + mouseEvent.getPoint().getX() - dragLocation.getX()),
                                slideComponentView.getHeight()
                        ));
                    }

                    dragLocation = mouseEvent.getPoint();
                    updateView();
                    break;
                }

                case (Cursor.S_RESIZE_CURSOR): {
                    for (SlideComponent component : slideComponentView.getSlideComponent().getSlide().getSelection().getSlideComponentList()) {
                        component.setSize(new Dimension(
                                slideComponentView.getWidth(),
                                (int) (slideComponentView.getHeight() + mouseEvent.getPoint().getY() - dragLocation.getY())
                        ));
                    }

                    dragLocation = mouseEvent.getPoint();
                    updateView();
                    break;
                }

                default: {
                    //for (SlideComponent component : slideComponentView.getSlideComponent().getSlide().getSelection().getSlideComponentList())
                    slideComponentView.setLocation(position);
                }
            }

            if (slideComponentView.isOverbearing()) {
                slideComponentView.getParent().setComponentZOrder(slideComponentView, 0);
                slideComponentView.update();
            }
        }

        @Override
        public void mouseMoved(MouseEvent mouseEvent) {
            Rectangle rectangle = slideComponentView.getVisibleRect();
            Integer shift = 5;

            if (mouseEvent.getY() > rectangle.getY() + rectangle.getHeight() - shift
                    && mouseEvent.getX() > rectangle.getX() + rectangle.getWidth() - shift) {
                slideComponentView.setCursor(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
            } else if (mouseEvent.getX() > rectangle.getX() + rectangle.getWidth() - shift) {
                slideComponentView.setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
            } else if (mouseEvent.getY() > rectangle.getY() + rectangle.getHeight() - shift) {
                slideComponentView.setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
            } else {
                slideComponentView.setCursor(slideComponentView.getDraggingCursor());
            }

            slideComponentView.setAnchorPoint(mouseEvent.getPoint());
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            dragging = true;
            dragLocation = mouseEvent.getPoint();

            if (mouseEvent.isShiftDown()) {
                    Rectangle rect = new Rectangle(slideComponentView.getSlideComponent().getSize());
                if (rect.contains(mouseEvent.getPoint())) {
                        slideComponentView.getSlideComponent().getSlide().getSelection().add(slideComponentView.getSlideComponent());
                    }
                }
            else if (mouseEvent.isPopupTrigger()) {
                showPopup(mouseEvent);
            }

        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {
            dragging = false;

            if (mouseEvent.isPopupTrigger()) {
                showPopup(mouseEvent);
            }

            if (position == null)
                return;

            if (Objects.equals(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR), slideComponentView.getCursor())) {
                ArrayList<SlideComponent> list = new ArrayList<>();
                list.add(slideComponentView.getSlideComponent());
                MoveComponentAction moveComponentAction = new MoveComponentAction(list, position);
                slideComponentView.getSlideComponent().getSlide().getPresentation().getActionStack().push(moveComponentAction);
            }

            position = null;

        }

        private void showPopup(MouseEvent mouseEvent) {
            SlideComponentContextMenu menu = new SlideComponentContextMenu(slideComponentView, mouseEvent.getX(), mouseEvent.getY());
            menu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
        }
    }
}
