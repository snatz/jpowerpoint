package presentation.controller.slideComponents;

import presentation.model.slideComponents.TextComponentListener;
import presentation.view.slideComponents.TextComponentView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TextComponentController extends SlideComponentController implements TextComponentListener {
    private TextComponentView textComponentView;

    public TextComponentController(TextComponentView textComponentView) {
        super(textComponentView);

        if(!textComponentView.isMiniature())
            textComponentView.addMouseListener(new DoubleClickListener());

        textComponentView.getTextComponent().addTextComponentListener(this);
        textComponentView.getTextComponent().addSlideComponentListener(this);

        this.textComponentView = textComponentView;
    }

    @Override
    public void onTextChanged() {
        textComponentView.updateText();
    }


    @Override
    public void onFocusLost() {
        super.onFocusLost();
        textComponentView.endEdition();
    }

    @Override
    public void onRemoved() {
    }


    private class DoubleClickListener extends MouseAdapter implements ActionListener{
        MouseEvent lastEvent;
        Timer timer;
        private final int clickInterval = (Integer)Toolkit.getDefaultToolkit().getDesktopProperty("awt.multiClickInterval");

        DoubleClickListener() {
            this.timer = new Timer(clickInterval,this);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            timer.stop();
        }

        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() > 2) return;

            lastEvent = e;

            if (timer.isRunning()) {
                timer.stop();
                doubleClick( lastEvent );
            }
            else
            {
                timer.restart();
            }

        }

        void doubleClick(MouseEvent e){
            textComponentView.edit();
        }
    }
}
