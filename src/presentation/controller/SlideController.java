package presentation.controller;

import presentation.factory.ViewFactory;
import presentation.model.actions.CreateImageAction;
import presentation.model.slide.SlideListener;
import presentation.model.slideComponents.SlideComponent;
import presentation.model.slideComponents.SlideComponentListener;
import presentation.view.SlideView;
import presentation.view.contextMenus.ContextMenu;
import presentation.view.contextMenus.VignetteContextMenu;
import presentation.view.slideComponents.SlideComponentView;

import javax.activation.MimetypesFileTypeMap;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class SlideController implements SlideListener {
    private final SlideView slideView;

    public SlideController(SlideView slideView) {
        this.slideView = slideView;

        slideView.addMouseListener(new MiniatureMouseListener());
        new DropTarget(slideView, new SlideContainerDropListener());

        if(slideView.isMiniature()) {
            slideView.addMouseListener(new MiniatureMouseListener());
        }
        else {
            slideView.addMouseListener(new SlideMouseListener());
        }

        slideView.getSlide().addSlideListener(this);

        updateView();
    }


    public void updateView()
    {
        refreshComponentList();

        slideView.update();
    }

    public void refreshComponentList(){

        List<SlideComponentView> slideComponentViewsList = slideView.getSlideComponentViewsList();


        if(slideComponentViewsList.size() == slideView.getSlide().getSlideComponentList().size())
            return;

        slideComponentViewsList.clear();

        for(SlideComponent slideComponent : slideView.getSlide().getSlideComponentList())
        {
            SlideComponentView slideComponentView = ViewFactory.createSlideComponentView(slideComponent, slideView.isMiniature());

            slideComponentViewsList.add(slideComponentView);

            slideComponentView.getSlideComponent().addSlideComponentListener(new UpdaterSlideComponentListener());

            if(!slideView.isMiniature()) {
                slideComponentView.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        slideView.getSlide().getSelection().selectOnly(slideComponentView.getSlideComponent());
                    }
                });
            }
        }
    }

    @Override
    public void onSlideComponentAdded(SlideComponent slideComponent) {
        updateView();
    }

    @Override
    public void onSlideComponentRemoved(SlideComponent slideComponent) {
        updateView();
    }

    @Override
    public void onSlideComponentModified(SlideComponent slideComponent) {
        updateView();
    }

    @Override
    public void onSelectionChanged() {
        updateView();
    }

    @Override
    public void onFocus() {
        slideView.onFocus();
    }

    @Override
    public void onFocusLost() {
        slideView.onFocusLost();
    }

    private class MiniatureMouseListener extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            if (mouseEvent.isPopupTrigger())
                showPopup(mouseEvent);
        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {
            if (mouseEvent.isPopupTrigger())
                showPopup(mouseEvent);
        }

        private void showPopup(MouseEvent mouseEvent) {
            VignetteContextMenu menu = new VignetteContextMenu(slideView, mouseEvent.getX(), mouseEvent.getY());
            menu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }

    private class SlideMouseListener extends MouseAdapter  {
        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            if (mouseEvent.isPopupTrigger())
                showPopup(mouseEvent);

            slideView.getSlide().getSelection().clear();
        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {
            if (mouseEvent.isPopupTrigger())
                showPopup(mouseEvent);
        }

        private void showPopup(MouseEvent mouseEvent) {
            ContextMenu menu = new ContextMenu(slideView.getSlide().getPresentation(), mouseEvent.getX(), mouseEvent.getY());
            menu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
        }
    }

    public class UpdaterSlideComponentListener implements SlideComponentListener
    {
        @Override
        public void onFocus() {
        }

        @Override
        public void onFocusLost() {

        }

        @Override
        public void onRemoved() {
            updateView();
        }

        @Override
        public void onModified() {

        }
    }

    private class SlideContainerDropListener extends DropTargetAdapter {
        @Override
        public void drop(DropTargetDropEvent dropTargetDropEvent) {
            processDrop(dropTargetDropEvent);
            Transferable transferable = dropTargetDropEvent.getTransferable();
            DataFlavor[] flavors = transferable.getTransferDataFlavors();

            for (DataFlavor flavor : flavors) {
                try {
                    if (flavor.isFlavorJavaFileListType()) {
                        @SuppressWarnings("unchecked")
                        List<File> files = (List<File>) transferable.getTransferData(flavor);
                        MimetypesFileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();

                        for (File file : files) {
                            String mimetype = mimetypesFileTypeMap.getContentType(file);
                            if (mimetype.startsWith("image")) {
                                CreateImageAction createImageAction = new CreateImageAction(
                                        slideView.getSlide().getPresentation(),
                                        file.getPath(),
                                        dropTargetDropEvent.getLocation().x,
                                        dropTargetDropEvent.getLocation().y
                                );
                                slideView.getSlide().getPresentation().getActionStack().push(createImageAction);
                            }
                        }
                    }
                } catch (UnsupportedFlavorException | IOException e){
                    e.printStackTrace();
                }
            }

            dropTargetDropEvent.dropComplete(true);
        }

        private void processDrop(DropTargetDropEvent dropTargetDropEvent) {
            if (dropTargetDropEvent.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                dropTargetDropEvent.acceptDrop(DnDConstants.ACTION_COPY);
            } else {
                dropTargetDropEvent.rejectDrop();
            }
        }
    }
}
