package presentation.controller;

import presentation.factory.ViewFactory;
import presentation.model.presentation.Presentation;
import presentation.utile.SaveManager;
import presentation.view.PresentationMenuView;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class PresentationMenuController {
    PresentationMenuController(PresentationMenuView menuView) {
        menuView.getLoadPres().addActionListener(e -> {
            JFileChooser chooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("XML Files", "xml");
            chooser.setFileFilter(filter);
            if (chooser.showOpenDialog(menuView) == JFileChooser.APPROVE_OPTION) {
                SaveManager saveManager = new SaveManager(chooser.getSelectedFile().getName(), menuView.getPresentation());
                saveManager.loadPresentation();
                ViewFactory.createPresentationFrameView(saveManager.getPresentation());
            }
        });

        menuView.getNewPres().addActionListener(e -> ViewFactory.createPresentationFrameView(new Presentation()));

        menuView.getSavePres().addActionListener(e -> {
            JFileChooser chooser = new JFileChooser();
            if (chooser.showOpenDialog(menuView) == JFileChooser.APPROVE_OPTION) {
                SaveManager saveManager = new SaveManager(chooser.getSelectedFile().getAbsolutePath() + ".xml", menuView.getPresentation());
                saveManager.savePresentation();
            }
        });
    }
}
