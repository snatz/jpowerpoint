package presentation.controller;

import presentation.model.presentation.PresentationListener;
import presentation.model.slide.Slide;
import presentation.view.SlideView;
import presentation.view.VignetteListView;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class VignetteListController implements PresentationListener {
    private VignetteListView vignetteListView;

    public VignetteListController(VignetteListView vignetteListView) {
        this.vignetteListView = vignetteListView;

        vignetteListView.getPresentation().addPresentationListener(this);
        vignetteListView.getPresentation().getSlideList().forEach(this::addSlide);

        updateView();
    }

    private void updateView() {
        vignetteListView.update();
    }

    @Override
    public void onSlideAdded(Slide slide) {
        addSlide(slide);
    }

    @Override
    public void onSlideRemoved(Slide slide) {
        removeSlide(slide);
    }

    @Override
    public void onSlideFocused(Slide slide) {
        updateView();
    }

    public void addSlide(Slide slide){
        SlideView vignetteView = new SlideView(slide, true);
        new SlideController(vignetteView);

        vignetteListView.getVignetteViewList().add(vignetteView);

        vignetteView.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                vignetteListView.getPresentation().setCurrentSlide(vignetteView.getSlide());
            }
        });

        updateView();
    }

    public void removeSlide(Slide slide)
    {
        SlideView vignetteView = vignetteListView.getSlideView(slide);

        if (vignetteView != null)
            vignetteListView.getVignetteViewList().remove(vignetteView);

        updateView();
    }

    public VignetteListView getVignetteListView() {
        return vignetteListView;
    }

    public void setVignetteListView(VignetteListView vignetteListView) {
        this.vignetteListView = vignetteListView;
    }
}
