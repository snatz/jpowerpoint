package presentation.controller;

import presentation.model.presentation.PresentationListener;
import presentation.model.slide.Slide;
import presentation.model.slideComponents.SlideComponent;
import presentation.view.PresentationFrameView;
import presentation.view.SlideContainerView;
import presentation.view.SlideView;
import presentation.view.slideComponents.SlideComponentView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class SlideContainerController implements PresentationListener {

    private SlideContainerView slideContainerView;
    private Slide currentSlide;

    public SlideContainerController(SlideContainerView slideContainerView) {

        this.slideContainerView = slideContainerView;

        //slideContainerView.addMouseListener(new SlideContainerMouseListener());
        //new DropTarget(slideContainerView, new SlideContainerDropListener());

        slideContainerView.getPresentation().addPresentationListener(this);
        setCurrentSlide(slideContainerView.getPresentation().getCurrentSlide());
        addAutoResize();

    }

    private void addAutoResize() {
        JFrame frame = (JFrame) SwingUtilities.windowForComponent(slideContainerView);

        if(frame != null) {
            frame.addComponentListener(new ResizeListener());
            computeZoomFactor();
        }
    }

    @Override
    public void onSlideAdded(Slide slide) {

    }

    @Override
    public void onSlideRemoved(Slide slide) {

    }

    @Override
    public void onSlideFocused(Slide slide) {
        setCurrentSlide(slide);
    }

    public void setCurrentSlide(Slide slide) {
        if(slide == currentSlide)
            return;

        slideContainerView.removeAll();

        if(slide == null)
            return;


        SlideView slideView = new SlideView(slide);
        new SlideController(slideView);

        slideContainerView.setSlideView(slideView);
        slideContainerView.add(slideView);
        slideContainerView.update();

        currentSlide = slide;
    }

    public SlideContainerView getSlideContainerView() {
        return slideContainerView;
    }

    private class ResizeListener extends ComponentAdapter {
        @Override
        public void componentResized(ComponentEvent e) {
            computeZoomFactor();
        }
    }

    private void computeZoomFactor() {
        PresentationFrameView frame = (PresentationFrameView) SwingUtilities.windowForComponent(slideContainerView);
        Dimension maxSize = frame.getSize();
        maxSize = new Dimension((int) maxSize.getWidth() - 300, (int) maxSize.getHeight() - 200);

        double maxSizeXRatio = maxSize.getWidth() / SlideView.INITIAL_DIMENSION.getWidth();
        double maxSizeYRatio = maxSize.getHeight() / SlideView.INITIAL_DIMENSION.getHeight();

        slideContainerView.setZoomRatio(Math.min(maxSizeXRatio, maxSizeYRatio));

        if(slideContainerView.getSlideView().getSlideComponentViewsList() != null) {
            for (SlideComponentView slideComponentView : slideContainerView.getSlideView().getSlideComponentViewsList()) {
                slideComponentView.setZoomFactor(Math.min(maxSizeXRatio, maxSizeYRatio));
            }
        }

        slideContainerView.update();
    }

}
