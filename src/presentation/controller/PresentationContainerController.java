package presentation.controller;

import presentation.controller.toolbarComponents.ToolbarController;
import presentation.model.actions.CreateImageAction;
import presentation.model.actions.CreateTextAction;
import presentation.model.actions.DeleteComponentAction;
import presentation.model.slide.Slide;
import presentation.model.slideComponents.ImageComponent;
import presentation.model.slideComponents.SlideComponent;
import presentation.model.slideComponents.TextComponent;
import presentation.view.PresentationContainerView;
import presentation.view.SlideContainerView;
import presentation.view.VignetteListView;
import presentation.view.toolbarComponents.Toolbar;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class PresentationContainerController {
    private PresentationContainerView presentationContainerView;
    private boolean isFullscreen = false;
    private List<SlideComponent> slideComponentListCopy = new ArrayList<>();

    public PresentationContainerController(PresentationContainerView presentationContainerView) {
        this.presentationContainerView = presentationContainerView;

        addShortcuts();

        SlideContainerView slideContainerView = presentationContainerView.getSlideContainerView();
        VignetteListView vignetteListView = presentationContainerView.getVignetteListView();
        Toolbar toolbar = presentationContainerView.getToolbar();

        new SlideContainerController(slideContainerView);
        new VignetteListController(vignetteListView);
        new ToolbarController(toolbar);
    }

    private void addShortcuts() {
        InputMap inputMap = presentationContainerView.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        ActionMap actionMap = presentationContainerView.getActionMap();

        inputMap.put(KeyStroke.getKeyStroke("control Z"), "undo");
        actionMap.put("undo", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                presentationContainerView.getPresentation().getActionStack().pop();
            }
        });

        inputMap.put(KeyStroke.getKeyStroke("control Y"), "redo");
        actionMap.put("redo", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                presentationContainerView.getPresentation().getActionStack().redo();
            }
        });

        inputMap.put(KeyStroke.getKeyStroke("control C"), "copy");
        actionMap.put("copy", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (presentationContainerView.getPresentation().getCurrentSlide().getSelection().getSlideComponentList().size() > 0
                        && !(slideComponentListCopy.containsAll(presentationContainerView.getPresentation().getCurrentSlide().getSelection().getSlideComponentList()))) {
                    slideComponentListCopy.clear();
                    slideComponentListCopy.addAll(presentationContainerView.getPresentation().getCurrentSlide().getSelection().getSlideComponentList());
                }
            }
        });

        inputMap.put(KeyStroke.getKeyStroke("control X"), "cut");
        actionMap.put("cut", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (presentationContainerView.getPresentation().getCurrentSlide().getSelection().getSlideComponentList().size() > 0
                        && !(slideComponentListCopy.containsAll(presentationContainerView.getPresentation().getCurrentSlide().getSelection().getSlideComponentList()))) {
                    slideComponentListCopy.clear();
                    slideComponentListCopy.addAll(presentationContainerView.getPresentation().getCurrentSlide().getSelection().getSlideComponentList());
                }
                slideComponentListCopy.forEach(SlideComponent::remove);
            }
        });

        inputMap.put(KeyStroke.getKeyStroke("control V"), "paste");
        actionMap.put("paste", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (slideComponentListCopy.size() == 0)
                    return;

                presentationContainerView.getPresentation().getComponentsToFocusPaste().clear();
                SlideComponent referentialComponent = slideComponentListCopy.get(0);
                for (SlideComponent slideComponent : slideComponentListCopy) {
                    if (slideComponent instanceof TextComponent) {
                        TextComponent textComponent = (TextComponent) slideComponent;
                        CreateTextAction createTextAction = new CreateTextAction(
                                textComponent.getText(),
                                textComponent.getFontSize(),
                                textComponent.getFontColor(),
                                textComponent.getFontName(),
                                textComponent.isItalic(),
                                textComponent.isBold(),
                                textComponent.isUnderline(),
                                presentationContainerView.getPresentation(),
                                presentationContainerView.getSlideContainerView().getMousePosition().x + textComponent.getPosition().x - referentialComponent.getPosition().x,
                                presentationContainerView.getSlideContainerView().getMousePosition().y + textComponent.getPosition().y - referentialComponent.getPosition().y
                        );
                        presentationContainerView.getPresentation().getActionStack().push(createTextAction);

                    } else if (slideComponent instanceof ImageComponent) {
                        ImageComponent imageComponent = (ImageComponent) slideComponent;
                        CreateImageAction createImageAction = new CreateImageAction(
                                presentationContainerView.getPresentation(),
                                imageComponent.getImagePath(),
                                presentationContainerView.getSlideContainerView().getMousePosition().x + imageComponent.getPosition().x - referentialComponent.getPosition().x,
                                presentationContainerView.getSlideContainerView().getMousePosition().y + imageComponent.getPosition().y - referentialComponent.getPosition().y
                        );
                        presentationContainerView.getPresentation().getActionStack().push(createImageAction);
                    }
                }

                presentationContainerView.getPresentation().getCurrentSlide().getSelection().clear();
                for(SlideComponent slideComponentToSetFocusOn : presentationContainerView.getPresentation().getComponentsToFocusPaste()){
                    presentationContainerView.getPresentation().getCurrentSlide().getSelection().add(slideComponentToSetFocusOn);
                }
            }
        });

        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "delete");
        actionMap.put("delete", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                SlideComponent componentToDelete = presentationContainerView.getPresentation().getCurrentSlide().getSelection().getSlideComponentList().get(0);
                DeleteComponentAction deleteComponentAction = new DeleteComponentAction(componentToDelete);
                presentationContainerView.getPresentation().getActionStack().push(deleteComponentAction);
            }
        });

        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F11, 0), "fullscreen");
        actionMap.put("fullscreen", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                isFullscreen = !isFullscreen;
                JFrame frame = presentationContainerView.getFrame();
                if (isFullscreen) {
                    frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
                    frame.dispose();
                    frame.setUndecorated(true);
                    presentationContainerView.getVignetteListView().setVisible(false);
                    presentationContainerView.getToolbar().setVisible(false);
                    frame.getJMenuBar().setVisible(false);
                    frame.setVisible(true);
                } else {
                    frame.setExtendedState(JFrame.NORMAL);
                    frame.dispose();
                    frame.setUndecorated(false);
                    presentationContainerView.getVignetteListView().setVisible(true);
                    presentationContainerView.getToolbar().setVisible(true);
                    frame.getJMenuBar().setVisible(true);
                    frame.setSize(Toolkit.getDefaultToolkit().getScreenSize().width, (Toolkit.getDefaultToolkit().getScreenSize().height) - 50);
                    frame.setVisible(true);
                }
            }
        });

        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "next slide");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "next slide");
        actionMap.put("next slide", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (isFullscreen)
                    presentationContainerView.getPresentation().setCurrentSlide(presentationContainerView.getPresentation().getNextSlide());
            }
        });

        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "previous slide");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "previous slide");
        actionMap.put("previous slide", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (isFullscreen)
                    presentationContainerView.getPresentation().setCurrentSlide(presentationContainerView.getPresentation().getPreviousSlide());
            }
        });
    }
}
