package presentation.model.slideComponents;

import presentation.model.slide.Slide;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

abstract public class SlideComponent implements Serializable {
    private Point position = new Point(0, 0);
    private Dimension size = new Dimension(50, 50);
    private List<SlideComponentListener> slideComponentListeners = new ArrayList<>();
    private Slide slide;

    public SlideComponent() {

    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        if(this.position == position)
            return;

        this.position = position;
        fireModified();
    }

    public Dimension getSize() {
        return size;
    }

    public void setSize(Dimension size) {
        this.size = size;
    }



    public void addSlideComponentListener(SlideComponentListener slideComponentListener) {
        slideComponentListeners.add(slideComponentListener);
    }

    public void removeSlideComponentListener(SlideComponentListener slideComponentListener) {
        slideComponentListeners.remove(slideComponentListener);
    }

    public void fireFocus() {
        slideComponentListeners.forEach(SlideComponentListener::onFocus);
    }

    public void fireFocusLost() {
        slideComponentListeners.forEach(SlideComponentListener::onFocusLost);
    }

    public void fireRemoved(){
        slideComponentListeners.forEach(SlideComponentListener::onRemoved);
    }

    public void fireModified() {
        slideComponentListeners.forEach(SlideComponentListener::onModified);
    }

    public boolean isFocused() {
        return getSlide() != null && getSlide().getSelection().getSlideComponentList().contains(this);
    }

    public Slide getSlide() {
        return slide;
    }

    public void setSlide(Slide slide) {
        this.slide = slide;
    }

    public void remove() {
        fireRemoved();
    }


}
