package presentation.model.slideComponents;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class TextComponent extends SlideComponent {
    private transient List<TextComponentListener> textComponentListeners = new ArrayList<>();
    private String text;
    private Integer fontSize = 11;
    private Color fontColor = Color.BLACK;
    private String fontName = "Arial";
    private boolean isItalic = false;
    private boolean isBold = false;
    private boolean isUnderline = false;

    public TextComponent() {
        text = "Insert text";
    }

    public TextComponent(String text) {
        setText(text);
    }

    public TextComponent(String text,Integer fontSize, Color fontColor, String fontName, boolean isItalic, boolean isBold, boolean isUnderline){
        setText(text);
        this.fontSize = fontSize;
        this.fontColor = fontColor;
        this.fontName = fontName;
        this.isItalic = isItalic;
        this.isBold = isBold;
        this.isUnderline = isUnderline;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        fireTextChanged();
    }

    public void modifyFont(Integer fontSize, Color fontColor, String fontName, boolean isItalic, boolean isBold, boolean isUnderline){
        this.fontSize = fontSize;
        this.fontColor = fontColor;
        this.fontName = fontName;
        this.isItalic = isItalic;
        this.isBold = isBold;
        this.isUnderline = isUnderline;
        fireTextChanged();
    }

    public void fireTextChanged() {
        textComponentListeners.forEach(TextComponentListener::onTextChanged);
    }

    public void addTextComponentListener(TextComponentListener textComponentListener) {
        textComponentListeners.add(textComponentListener);
    }

    public void removeTextComponentListener(TextComponentListener textComponentListener) {
        textComponentListeners.remove(textComponentListener);
    }

    public void setFontSize(Integer fontSize) {
        this.fontSize = fontSize;
    }

    public Color getFontColor() {
        return fontColor;
    }

    public String getFontName() {
        return fontName;
    }

    public Integer getFontSize() {
        return fontSize;
    }

    public boolean isItalic() { return isItalic; }

    public void setFontColor(Color fontColor) {
        this.fontColor = fontColor;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    public void setItalic(boolean italic) {
        isItalic = italic;
    }

    public void setBold(boolean bold) {
        isBold = bold;
    }

    public void setUnderline(boolean underline) {
        isUnderline = underline;
    }

    public boolean isBold() { return isBold; }

    public boolean isUnderline() { return isUnderline; }
}
