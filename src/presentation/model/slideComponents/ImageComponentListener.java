package presentation.model.slideComponents;

public interface ImageComponentListener extends SlideComponentListener {
    void onImageChange();
}
