package presentation.model.slideComponents;

public interface SlideComponentListener {
    void onFocus();
    void onFocusLost();
    void onRemoved();
    void onModified();
}
