package presentation.model.slideComponents;

public interface TextComponentListener extends SlideComponentListener {
    void onTextChanged();
}
