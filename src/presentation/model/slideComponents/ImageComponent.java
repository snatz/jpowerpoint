package presentation.model.slideComponents;

import java.util.ArrayList;
import java.util.List;

public class ImageComponent extends SlideComponent {
    private transient List<ImageComponentListener> imageComponentListeners = new ArrayList<>();
    private String imagePath;

    public ImageComponent() {

    }

    public ImageComponent(String path) {
        setImagePath(path);
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
        fireImageChanged();
    }

    public void rotateImage(){

    }

    private void fireImageChanged() {
        imageComponentListeners.forEach(ImageComponentListener::onImageChange);
    }

    public void addImageComponentListener(ImageComponentListener imageComponentListener) {
        imageComponentListeners.add(imageComponentListener);
    }

    public void removeImageComponentListener(ImageComponentListener imageComponentListener) {
        imageComponentListeners.remove(imageComponentListener);
    }
}
