package presentation.model.selection;

public interface SelectionListener {
    void onSelectionChanged();
}
