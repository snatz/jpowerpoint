package presentation.model.selection;

import presentation.model.slideComponents.SlideComponent;
import presentation.model.slideComponents.SlideComponentListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Selection {
    private List<SlideComponent> slideComponentList = new ArrayList<>();
    private List<SelectionListener> selectionListeners = new ArrayList<>();

    public Selection() {

    }
    
    public void clear() {
        Iterator<SlideComponent> it = slideComponentList.iterator();

        while (it.hasNext()) {
            SlideComponent slideComponent = it.next();
            it.remove();

            slideComponent.fireFocusLost();
        }

        fireSelectionChanged();
    }

    public void selectOnly(SlideComponent toSelectComponent)
    {
        Iterator<SlideComponent> it = slideComponentList.iterator();

        // Remove while iterate in list

        while (it.hasNext()) {
            SlideComponent slideComponent = it.next();

            if(slideComponent == toSelectComponent)
                continue;

            it.remove();

            slideComponent.fireFocusLost();
        }

        // Add to selection

        add(toSelectComponent);

    }

    public void add(SlideComponent slideComponent)
    {
        if(slideComponentList.contains(slideComponent))
            return;

        slideComponentList.add(slideComponent);
        slideComponent.fireFocus();

        slideComponent.addSlideComponentListener(new SlideComponentListener() {
            @Override
            public void onFocus() {

            }

            @Override
            public void onFocusLost() {

            }

            @Override
            public void onRemoved() {
                slideComponentList.remove(slideComponent);
            }

            @Override
            public void onModified() {

            }
        });

        fireSelectionChanged();
    }

    public void remove(SlideComponent slideComponent)
    {
        slideComponentList.remove(slideComponent);
        slideComponent.fireFocusLost();

        fireSelectionChanged();
    }

    private void fireSelectionChanged(){
        selectionListeners.forEach(SelectionListener::onSelectionChanged);
    }

    public void addSelectionListener(SelectionListener selectionListener){
        selectionListeners.add(selectionListener);
    }

    public void removeSelectionListener(SelectionListener selectionListener){
        selectionListeners.remove(selectionListener);
    }

    public List<SlideComponent> getSlideComponentList() {
        return slideComponentList;
    }
}
