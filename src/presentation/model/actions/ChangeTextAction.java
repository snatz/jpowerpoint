package presentation.model.actions;

import presentation.model.slideComponents.TextComponent;
import presentation.view.toolbarComponents.ToolbarText;

import javax.swing.*;
import java.awt.*;

public class ChangeTextAction implements Action {
    private TextComponent textComponent;
    private ToolbarText toolbarText;
    private Color previousColor = Color.BLACK;
    private String previousFont, usage;
    private Integer previousSize;
    private Color newColor = Color.BLACK;
    private boolean previousItalic,previousBold,previousUnderline;

    public ChangeTextAction(TextComponent textComponent, ToolbarText toolbarText, String usage, Color newColor) {
        this.textComponent = textComponent;
        this.toolbarText = toolbarText;
        this.previousColor = null;
        this.previousFont = null;
        this.previousSize = null;
        this.previousItalic = false;
        this.previousBold = false;
        this.previousUnderline = false;
        this.usage = usage;
        this.newColor = newColor;
    }

    @Override
    public void invoke() {
        switch (usage) {
            case "color": {
                invokeColor();
                break;
            }

            case "font": {
                invokeFont();
                break;
            }

            case "size": {
                invokeSize();
                break;
            }

            case "italic": {
                invokeItalic();
                break;
            }
            case "bold":{
                invokeBold();
                break;
            }

            case "underline": {
                invokeUnderline();
                break;
            }
        }
    }

    @Override
    public void undo() {
        switch (usage) {
            case "color": {
                undoColor();
                break;
            }

            case "font": {
                undoFont();
                break;
            }

            case "size": {
                undoSize();
                break;
            }

            case "italic": {
                undoItalic();
                break;
            }
            case "bold": {
                undoBold();
                break;
            }

            case "underline": {
                undoUnderline();
                break;
            }
        }
    }

    private void launchModifyFont(){
        textComponent.modifyFont(textComponent.getFontSize(), textComponent.getFontColor(), textComponent.getFontName(), textComponent.isItalic(),textComponent.isBold(),textComponent.isUnderline());
    }

    private void invokeColor() {
        previousColor = textComponent.getFontColor();
        textComponent.setFontColor(newColor);
        toolbarText.getColorChooser().setBackground(textComponent.getFontColor());
        toolbarText.setCurrentColor(textComponent.getFontColor());
        launchModifyFont();
    }

    private void invokeFont() {
        previousFont = textComponent.getFontName();
        textComponent.setFontName((String) toolbarText.getFontCombo().getSelectedItem());
        toolbarText.setCurrentFontName(toolbarText.getCurrentFontName());
        launchModifyFont();
    }

    private void invokeSize() {
        try {
            previousSize = textComponent.getFontSize();
            textComponent.setFontSize(Integer.parseInt(toolbarText.getFontSizeArea().getText()));
            toolbarText.setCurrentFontSize(textComponent.getFontSize());
            launchModifyFont();
        } catch (NumberFormatException nfe) {
            toolbarText.getFontSizeArea().setText(textComponent.getFontSize().toString());
        }
    }

    private void invokeItalic() {
        previousItalic = textComponent.isItalic();
        textComponent.setItalic(!(textComponent.isItalic()));
        toolbarText.setItalic(textComponent.isItalic());
        launchModifyFont();
    }

    private void invokeBold(){
        previousBold = textComponent.isBold();
        textComponent.setBold(!(textComponent.isBold()));
        toolbarText.setBold(textComponent.isBold());
        launchModifyFont();
    }

    private void invokeUnderline(){
        previousUnderline = textComponent.isUnderline();
        textComponent.setUnderline(!(textComponent.isUnderline()));
        toolbarText.setUnderline(textComponent.isUnderline());
        launchModifyFont();
    }

    private void undoColor() {
        toolbarText.setCurrentColor(previousColor);
        toolbarText.getColorChooser().setBackground(toolbarText.getCurrentColor());
        previousColor = null;
        textComponent.setFontColor(toolbarText.getCurrentColor());
        launchModifyFont();
    }

    private void undoFont() {
        toolbarText.setCurrentFontName(previousFont);
        previousFont = null;
        textComponent.setFontName(toolbarText.getCurrentFontName());
        launchModifyFont();
    }

    private void undoSize() {
        try {
            toolbarText.setCurrentFontSize(previousSize);
            previousSize = null;
            textComponent.setFontSize(toolbarText.getCurrentFontSize());
            launchModifyFont();
        } catch (NumberFormatException nfe) {
            toolbarText.getFontSizeArea().setText(textComponent.getFontSize().toString());
        }
    }

    private void undoItalic() {
        toolbarText.setItalic(!previousItalic);
        previousItalic = !previousItalic;
        textComponent.setItalic(toolbarText.isItalic());
        launchModifyFont();
    }

    private void undoBold(){
        toolbarText.setBold(!previousBold);
        previousBold = !previousBold;
        textComponent.setBold(toolbarText.isBold());
        launchModifyFont();
    }

    private void undoUnderline(){
        toolbarText.setUnderline(!previousUnderline);
        previousUnderline = !previousUnderline;
        textComponent.setUnderline((toolbarText.isUnderline()));
        launchModifyFont();
    }
}


