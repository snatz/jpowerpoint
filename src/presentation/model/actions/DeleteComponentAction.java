package presentation.model.actions;

import presentation.model.slideComponents.SlideComponent;

public class DeleteComponentAction implements Action {
    private SlideComponent component;

    public DeleteComponentAction(SlideComponent component) {
        this.component = component;
    }

    @Override
    public void invoke() {
        component.remove();
    }

    @Override
    public void undo() {
        component.getSlide().addComponent(component);
    }
}
