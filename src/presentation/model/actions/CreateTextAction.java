package presentation.model.actions;

import presentation.model.presentation.Presentation;
import presentation.model.slideComponents.TextComponent;

import java.awt.*;

public class CreateTextAction implements Action {
    private Presentation presentation;
    private TextComponent textComponent;
    private int textX, textY;
    private String txt,fontName;
    private Integer fontSize;
    private Color fontColor;
    private boolean isItalic, isBold,isUnderline;

    public CreateTextAction(String txt,Integer fontSize, Color fontColor, String fontName, boolean isItalic, boolean isBold, boolean isUnderline,Presentation presentation, int textX, int textY){
        this.txt = txt;
        this.fontSize = fontSize;
        this.fontColor = fontColor;
        this.fontName = fontName;
        this.isItalic = isItalic;
        this.isBold = isBold;
        this.isUnderline = isUnderline;
        this.presentation = presentation;
        this.textX = textX;
        this.textY = textY;
    }

    public CreateTextAction(String txt,Presentation presentation, int textX, int textY){
        this(txt,11,Color.BLACK,"Arial",false,false,false,presentation,textX,textY);
    }

    public CreateTextAction(Presentation presentation, int textX, int textY) {
        this("",presentation,textX,textY);
    }

    @Override
    public void invoke() {
        if (txt.equals("")) {
            textComponent = new TextComponent();
        } else {
            textComponent = new TextComponent(txt,fontSize,fontColor,fontName,isItalic,isBold,isUnderline);
        }
        presentation.getComponentsToFocusPaste().add(textComponent);
        textComponent.setPosition(new Point(textX, textY));
        presentation.getCurrentSlide().addComponent(textComponent);
    }

    @Override
    public void undo() {
        textComponent.remove();
    }
}
