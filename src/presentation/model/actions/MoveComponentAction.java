package presentation.model.actions;

import presentation.model.slideComponents.SlideComponent;

import java.awt.*;
import java.util.List;

public class MoveComponentAction implements Action {
    private List<SlideComponent> slideComponentList;
    private Point previousPosition, newPosition;

    public MoveComponentAction(List<SlideComponent> slideComponentList, Point newPosition) {
        this.slideComponentList = slideComponentList;
        this.newPosition = newPosition;
        this.previousPosition = null;
    }

    @Override
    public void invoke() {
        for (SlideComponent slideComponent : slideComponentList) {
            previousPosition = slideComponent.getPosition();
            slideComponent.setPosition(newPosition);
            slideComponent.fireModified();
        }
    }

    @Override
    public void undo() {
        for (SlideComponent slideComponent : slideComponentList) {
            slideComponent.setPosition(previousPosition);
            previousPosition = null;
            slideComponent.fireModified();
        }
    }
}
