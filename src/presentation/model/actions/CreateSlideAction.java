package presentation.model.actions;

import presentation.model.presentation.Presentation;
import presentation.model.slide.Slide;

public class CreateSlideAction implements Action {
    private Presentation presentation;
    private Slide newSlide = null;

    public CreateSlideAction(Presentation presentation) {
        this.presentation = presentation;
    }

    @Override
    public void invoke() {
        newSlide = new Slide();
        presentation.addSlide(newSlide);
    }

    @Override
    public void undo() {
        presentation.removeSlide(newSlide);
        newSlide = null;
    }
}
