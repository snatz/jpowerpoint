package presentation.model.actions;

public interface Action {
    void invoke();
    void undo();
}
