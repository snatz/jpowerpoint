package presentation.model.actions;

import presentation.model.presentation.Presentation;
import presentation.model.slideComponents.ImageComponent;

import java.awt.*;

public class CreateImageAction implements Action {
    private Presentation presentation;
    private ImageComponent imageComponent;
    private int imageX, imageY;

    public CreateImageAction(Presentation presentation, String path, int imageX, int imageY) {
        this.presentation = presentation;
        this.imageComponent = new ImageComponent(path);
        this.imageX = imageX;
        this.imageY = imageY;
    }

    @Override
    public void invoke() {
        imageComponent.setPosition(new Point(imageX, imageY));
        presentation.getComponentsToFocusPaste().add(imageComponent);
        presentation.getCurrentSlide().addComponent(imageComponent);
    }

    @Override
    public void undo() {
        imageComponent.remove();
    }
}
