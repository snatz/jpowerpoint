package presentation.model.slide;

import presentation.model.presentation.Presentation;
import presentation.model.selection.Selection;
import presentation.model.selection.SelectionListener;
import presentation.model.slideComponents.SlideComponent;
import presentation.model.slideComponents.SlideComponentListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Slide implements Serializable {
    private transient List<SlideListener> slideListeners = new ArrayList<>();
    private List<SlideComponent> slideComponentList = new ArrayList<>();
    private transient Selection selection = new Selection();
    private boolean currentSlide;
    private Presentation presentation;

    public Slide() {
        // Forward the selection changed event
        selection.addSelectionListener(() -> fireSelectionChanged());
    }

    public void addComponent(SlideComponent slideComponent) {
        slideComponentList.add(slideComponent);
        slideComponent.setSlide(this);

        fireSlideComponentAdded(slideComponent);

        slideComponent.addSlideComponentListener(new SlideComponentListener() {
            @Override
            public void onFocus() {

            }

            @Override
            public void onFocusLost() {

            }

            @Override
            public void onRemoved() {
                slideComponentList.remove(slideComponent);
            }

            @Override
            public void onModified() {
                // forward event
                fireSlideComponentModified(slideComponent);
            }
        });

        selection.selectOnly(slideComponent);
    }

    @Deprecated
    public void removeComponent(SlideComponent slideComponent) {
        slideComponent.remove();
    }

    @Deprecated
    public void focusSlideComponent(SlideComponent slideComponent) {

    }

    public void remove() {
        presentation.removeSlide(this);
    }

    public Presentation getPresentation() {
        return presentation;
    }

    public void setPresentation(Presentation presentation) {
        this.presentation = presentation;
    }

    public Slide(List<SlideComponent> slideComponentList) {
        this.slideComponentList = slideComponentList;
    }

    public List<SlideComponent> getSlideComponentList() {
        return slideComponentList;
    }

    public void setSlideComponentList(List<SlideComponent> slideComponentList) {
        this.slideComponentList = slideComponentList;
    }

    public void addSlideListener(SlideListener slideListener)
    {
        slideListeners.add(slideListener);
    }

    public void removeSlideListener(SlideListener slideListener)
    {
        slideListeners.remove(slideListener);
    }

    public void fireSlideComponentAdded(SlideComponent slideComponent) {
        for(SlideListener slideListener : slideListeners)
        {
            slideListener.onSlideComponentAdded(slideComponent);
        }
    }

    public void fireSlideComponentRemoved(SlideComponent slideComponent) {
        for(SlideListener slideListener : slideListeners)
        {
            slideListener.onSlideComponentRemoved(slideComponent);
        }
    }

    public void fireSlideComponentModified(SlideComponent slideComponent) {
        for(SlideListener slideListener : slideListeners)
        {
            slideListener.onSlideComponentModified(slideComponent);
        }
    }

    public void fireSelectionChanged(){
        for(SlideListener slideListener : slideListeners)
        {
            slideListener.onSelectionChanged();
        }
    }

    public void fireSlideFocus()
    {
        slideListeners.forEach(SlideListener::onFocus);
    }

    public void fireSlideFocusLost() {
        slideListeners.forEach(SlideListener::onFocusLost);
    }

    public boolean isCurrentSlide() {
        return currentSlide;
    }

    public void setCurrentSlide(Boolean currentSlide) {
        this.currentSlide = currentSlide;
    }

    @Deprecated
    public SlideComponent getFocusedSlideComponent() {
        return null;
    }

    public Selection getSelection() {
        return selection;
    }

    public void setCurrentSlide(boolean currentSlide) {
        this.currentSlide = currentSlide;
    }


}
