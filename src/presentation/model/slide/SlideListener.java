package presentation.model.slide;

import presentation.model.slideComponents.SlideComponent;

public interface SlideListener {
    void onSlideComponentAdded(SlideComponent slideComponent);
    void onSlideComponentRemoved(SlideComponent slideComponent);
    void onSlideComponentModified(SlideComponent slideComponent);
    void onSelectionChanged();

    void onFocus();
    void onFocusLost();


}
