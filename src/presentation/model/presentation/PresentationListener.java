package presentation.model.presentation;

import presentation.model.slide.Slide;

public interface PresentationListener {
    void onSlideAdded(Slide slide);
    void onSlideRemoved(Slide slide);
    void onSlideFocused(Slide slide);
}
