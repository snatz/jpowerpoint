package presentation.model.presentation;

import presentation.model.actions.ActionStack;
import presentation.model.slide.Slide;
import presentation.model.slideComponents.SlideComponent;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Presentation implements Serializable {
    private List<PresentationListener> presentationListeners = new ArrayList<>();
    private List<Slide> slideList;
    private Slide currentSlide;
    private ActionStack actionStack;
    private List<SlideComponent> componentsToFocusPaste = new ArrayList<>();

    public Presentation() {
        slideList = new ArrayList<>();

        // By default, add a empty slide
        Slide defaultSlide = new Slide();

        addSlide(defaultSlide);

        actionStack = new ActionStack();
    }

    public void addSlide(Slide slide) {
        slide.setPresentation(this);
        slideList.add(slide);

        fireSlideAdded(slide);

        setCurrentSlide(slide);
    }

    public void removeSlide(Slide slide) {
        if (slideList.size() <= 1) // Can't delete last slide
            return;

        slide.setPresentation(null);
        slideList.remove(slide);

        fireSlideRemoved(slide);
    }

    public ActionStack getActionStack() {
        return actionStack;
    }

    public Slide getCurrentSlide() {
        return currentSlide;
    }

    public void setCurrentSlide(Slide slide) {
        if(currentSlide == slide)
            return;

        if(currentSlide != null) {
            currentSlide.setCurrentSlide(false);
            currentSlide.fireSlideFocusLost();
        }

        currentSlide = slide;

        if(slide != null){
            slide.setCurrentSlide(true);
            slide.fireSlideFocus();
        }

        fireSlideFocused(slide);
    }

    public void addPresentationListener(PresentationListener presentationListener) {
        presentationListeners.add(presentationListener);
    }

    public void removePresentationListener(PresentationListener presentationListener) {
        presentationListeners.remove(presentationListener);
    }

    public void fireSlideAdded(Slide slide){
        for(PresentationListener presentationListener : presentationListeners)
        {
            presentationListener.onSlideAdded(slide);
        }
    }

    public void fireSlideRemoved(Slide slide) {
        for(PresentationListener presentationListener : presentationListeners)
        {
            presentationListener.onSlideRemoved(slide);
        }
    }

    public void fireSlideFocused(Slide slide) {
        for(PresentationListener presentationListener : presentationListeners)
        {
            presentationListener.onSlideFocused(slide);
        }
    }

    public List<Slide> getSlideList() {
        return slideList;
    }

    public void setSlideList(List<Slide> slideList) {
        this.slideList = slideList;
    }

    public Slide getSlide(Integer index) {
        return slideList.get(index);
    }

    public List<SlideComponent> getComponentsToFocusPaste() {
        return componentsToFocusPaste;
    }

    public void setComponentsToFocusPaste(List<SlideComponent> componentsToFocusPaste) {
        this.componentsToFocusPaste = componentsToFocusPaste;
    }

    public Slide getNextSlide() {
        int slideIndex = slideList.indexOf(getCurrentSlide());
        return (slideIndex + 1 < slideList.size() ? slideList.get(slideIndex + 1) : getCurrentSlide());
    }

    public Slide getPreviousSlide() {
        int slideIndex = slideList.indexOf(getCurrentSlide());
        return (slideIndex - 1 >= 0 ? slideList.get(slideIndex - 1) : getCurrentSlide());
    }
}
