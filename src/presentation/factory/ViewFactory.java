package presentation.factory;

import presentation.controller.PresentationFrameController;
import presentation.model.presentation.Presentation;
import presentation.model.slideComponents.SlideComponent;
import presentation.view.PresentationFrameView;
import presentation.view.slideComponents.SlideComponentView;

import java.lang.reflect.InvocationTargetException;

public class ViewFactory {

    public static PresentationFrameView createPresentationFrameView(Presentation presentation)
    {
        PresentationFrameView presentationFrameView = new PresentationFrameView(presentation);
        new PresentationFrameController(presentationFrameView);

        return presentationFrameView;
    }

    public static SlideComponentView createSlideComponentView(SlideComponent slideComponent)
    {
        return createSlideComponentView(slideComponent, false);
    }

    public static SlideComponentView createSlideComponentView(SlideComponent slideComponent, boolean miniature) {
        Object view = null;

        try {
            String viewString = slideComponent.getClass().getName() + "View";
            String controllerString = slideComponent.getClass().getName() + "Controller";

            viewString = viewString.replaceAll("model", "view");
            controllerString = controllerString.replaceAll("model", "controller");

            view = Class.forName(viewString).getDeclaredConstructor(slideComponent.getClass(), boolean.class).newInstance(slideComponent, miniature);
            Object controller = Class.forName(controllerString).getDeclaredConstructor(view.getClass()).newInstance(view);
        } catch (InstantiationException | ClassNotFoundException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return (SlideComponentView) view;
    }
}
